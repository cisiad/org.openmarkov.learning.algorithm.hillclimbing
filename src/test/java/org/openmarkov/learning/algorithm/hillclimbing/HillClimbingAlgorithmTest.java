/*
* Copyright 2011 CISIAD, UNED, Spain
*
* Licensed under the European Union Public Licence, version 1.1 (EUPL)
*
* Unless required by applicable law, this code is distributed
* on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
*/

package org.openmarkov.learning.algorithm.hillclimbing;


import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openmarkov.core.io.database.CaseDatabase;
import org.openmarkov.core.model.network.NodeType;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.Node;
import org.openmarkov.core.model.network.Variable;
import org.openmarkov.core.model.network.potential.TablePotential;
import org.openmarkov.core.model.network.potential.operation.DiscretePotentialOperations;
import org.openmarkov.io.database.elvira.ElviraDataBaseIO;
import org.openmarkov.learning.algorithm.scoreAndSearch.metric.Metric;
import org.openmarkov.learning.core.util.ModelNetUse;
import org.openmarkov.learning.metric.bayesian.BayesianMetric;


public class HillClimbingAlgorithmTest {
	
    private String dbFilename = "/learnTestDataBase.dbc";
	
	private final double maxError = 1E-6;
	
	private double alpha = 0.5;
	
	private HillClimbingAlgorithm learningAlgorithm;
	private ProbNet learned;
	private Metric metric;
	
	@Before
	public void setUp() throws Exception {
		
        ElviraDataBaseIO databaseIO = new ElviraDataBaseIO ();
        CaseDatabase database = databaseIO.load(getClass().getResource (dbFilename).getFile ());
        learned = new ProbNet ();
        for(Variable variable : database.getVariables ())
        {
            learned.addNode (variable, NodeType.CHANCE);
        }
        
        metric = new BayesianMetric(alpha);
		
        learningAlgorithm = new HillClimbingAlgorithm (learned, database, alpha, metric);
	}

	@Test
	public void testLearning() throws Exception {
		double[] probabilities;
        learningAlgorithm.run (new ModelNetUse());
        Node nodeA = learned.getNode ("A");
        Node nodeB = learned.getNode ("B");
        Node nodeC = learned.getNode ("C");
        Node nodeD = learned.getNode ("D");
        Node nodeE = learned.getNode ("E");
        Node nodeF = learned.getNode ("F");
        Variable variableA = nodeA.getVariable();
        Variable variableB = nodeB.getVariable();
        Variable variableC = nodeC.getVariable();
        Variable variableD = nodeD.getVariable();
        Variable variableE = nodeE.getVariable();
        Variable variableF = nodeF.getVariable();
        
        // check the structure of the learned net
        // present links
        Assert.assertTrue (nodeA.isParent (nodeB));
        Assert.assertTrue (nodeC.isParent (nodeB));
        Assert.assertTrue (nodeC.isParent (nodeA));
        Assert.assertTrue (nodeA.isParent (nodeD));
        Assert.assertTrue (nodeC.isParent (nodeD));
        Assert.assertTrue (nodeD.isParent (nodeE));
        // non-present links
        Assert.assertFalse (nodeB.isParent (nodeA));
        Assert.assertFalse (nodeA.isParent (nodeC));
        Assert.assertFalse (nodeD.isParent (nodeA));
        Assert.assertFalse (nodeE.isParent (nodeA));
        Assert.assertFalse (nodeF.isParent (nodeA));
        Assert.assertFalse (nodeD.isParent (nodeB));
        Assert.assertFalse (nodeE.isParent (nodeB));
        Assert.assertFalse (nodeF.isParent (nodeB));
        Assert.assertFalse (nodeB.isParent (nodeC));
        Assert.assertFalse (nodeD.isParent (nodeC));
        Assert.assertFalse (nodeE.isParent (nodeC));
        Assert.assertFalse (nodeF.isParent (nodeC));
        Assert.assertFalse (nodeB.isParent (nodeD));
        Assert.assertFalse (nodeE.isParent (nodeD));
        Assert.assertFalse (nodeF.isParent (nodeD));
        Assert.assertFalse (nodeA.isParent (nodeE));
        Assert.assertFalse (nodeB.isParent (nodeE));
        Assert.assertFalse (nodeC.isParent (nodeE));
        Assert.assertFalse (nodeF.isParent (nodeE));
        Assert.assertFalse (nodeA.isParent (nodeF));
        Assert.assertFalse (nodeB.isParent (nodeF));
        Assert.assertFalse (nodeC.isParent (nodeF));
        Assert.assertFalse (nodeD.isParent (nodeF));
        Assert.assertFalse (nodeE.isParent (nodeF));
        // chek the CPTs
        TablePotential aGivenBDPotential = (TablePotential) nodeA.getPotentials ().get (0);
        List<Variable> aGivenBDVariables = Arrays.asList(variableA, variableB, variableD);
        aGivenBDPotential = DiscretePotentialOperations.reorder(aGivenBDPotential, aGivenBDVariables);
        probabilities = aGivenBDPotential.getValues ();
        Assert.assertEquals (0.022135, probabilities[0], maxError);
        Assert.assertEquals (0.977864, probabilities[1], maxError);
        Assert.assertEquals (0.316326, probabilities[2], maxError);
        Assert.assertEquals (0.683673, probabilities[3], maxError);
        Assert.assertEquals (0.776018, probabilities[4], maxError);
        Assert.assertEquals (0.223981, probabilities[5], maxError);
        Assert.assertEquals (0.321428, probabilities[6], maxError);
        Assert.assertEquals (0.678571, probabilities[7], maxError);
        probabilities = ((TablePotential) nodeB.getPotentials ().get (0)).getValues ();
        Assert.assertEquals (0.602897, probabilities[0], maxError);
        Assert.assertEquals (0.397102, probabilities[1], maxError);
        TablePotential cGivenBDAPotential = (TablePotential) nodeC.getPotentials ().get (0);
        List<Variable> cGivenBDAVariables = Arrays.asList(variableC, variableB, variableD, variableA);
        cGivenBDAPotential = DiscretePotentialOperations.reorder(cGivenBDAPotential, cGivenBDAVariables);
        probabilities = cGivenBDAPotential.getValues ();
        Assert.assertEquals (0.277777, probabilities[0], maxError);
        Assert.assertEquals (0.722222, probabilities[1], maxError);
        Assert.assertEquals (0.698717, probabilities[2], maxError);
        Assert.assertEquals (0.301282, probabilities[3], maxError);
        Assert.assertEquals (0.287790, probabilities[4], maxError);
        Assert.assertEquals (0.712209, probabilities[5], maxError);
        Assert.assertEquals (0.19, probabilities[6], maxError);
        Assert.assertEquals (0.81, probabilities[7], maxError);
        Assert.assertEquals (0.780585, probabilities[8], maxError);
        Assert.assertEquals (0.219414, probabilities[9], maxError);
        Assert.assertEquals (0.205357, probabilities[10], maxError);
        Assert.assertEquals (0.794642, probabilities[11], maxError);
        Assert.assertEquals (0.87, probabilities[12], maxError);
        Assert.assertEquals (0.13, probabilities[13], maxError);
        Assert.assertEquals (0.719047, probabilities[14], maxError);
        Assert.assertEquals (0.280952, probabilities[15], maxError);
        probabilities = ((TablePotential) nodeD.getPotentials ().get (0)).getValues ();
        Assert.assertEquals (0.177685, probabilities[0], maxError);
        Assert.assertEquals (0.822314, probabilities[1], maxError);
        Assert.assertEquals (0.881846, probabilities[2], maxError);
        Assert.assertEquals (0.118153, probabilities[3], maxError);
        probabilities = ((TablePotential) nodeE.getPotentials ().get (0)).getValues ();
        Assert.assertEquals (0.362137, probabilities[0], maxError);
        Assert.assertEquals (0.637862, probabilities[1], maxError);
        probabilities = ((TablePotential) nodeF.getPotentials ().get (0)).getValues ();
        Assert.assertEquals (0.500999, probabilities[0], maxError);
        Assert.assertEquals (0.499000, probabilities[1], maxError);
	}
}
